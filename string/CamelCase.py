
def count_camel_case(cc_str):
    count = 0
    for i, c in enumerate(cc_str):
        if c.isupper():
            if i > 0 and count is 0:
                count = 2
                continue
            count += 1
    return count

s = str(input())
t = count_camel_case(s)
if t is 0:
    t=1
print(t)
