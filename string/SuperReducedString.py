

def reduce_string(s):
    pre = ''
    for i, c in enumerate(s):
        if c is pre:
            s = s.replace(pre+c, "")
            #print(s)
            return s
        pre = c
    return 'no_adjacent'

s = input()

while 1:
    result = reduce_string(s)
    if not result:
        print('Empty String')
        break
    elif result is 'no_adjacent':
        print(s)
        break
    s = result


