alphabets = [chr(i) for i in range(ord('a'),ord('z')+1)]
al_with_value = dict((alphabets[i], i+1) for i in range(0, len(alphabets)))

n = int(input())

for i in range(n):
    exam_str = str(input())
    str_len = len(exam_str)
    flag = True
    for i in range(1, str_len):
        x = i
        y = i-1
        rx = str_len - (i+1)
        ry = str_len - i
        val_x = al_with_value.get(exam_str[x], 0)
        val_y = al_with_value.get(exam_str[y], 0)
        val_rx = al_with_value.get(exam_str[rx], 0)
        val_ry = al_with_value.get(exam_str[ry], 0)

        if abs(val_x - val_y) is not abs(val_rx - val_ry) :
            flag = False
            break
    if flag:
        print('Funny')
    else:
        print('Not Funny')
