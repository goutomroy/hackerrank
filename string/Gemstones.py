n = int(input())
stones = []
shortest_len = 1001
for i in range(n):
    each_stone = str(input())
    if len(each_stone) < shortest_len :
        shortest_len = len(each_stone)
        shortest_str = each_stone
    stones.append(each_stone)

count = 0
unique_char_set = list(set(shortest_str))
for c in unique_char_set:

    flag = True
    for each_stone in stones:
        if each_stone is shortest_str:
            continue
        if c not in each_stone:
            flag = False
            break
    if flag :
        count = count + 1

print(count)


