import collections

m = int(input())
str_a = str(input())
a = list(map(int,str_a.split()))


n = int(input())
str_b = str(input())
b = list(map(int,str_b.split()))


counter_a = dict(collections.Counter(a))
counter_b = dict(collections.Counter(b))

results = []
for each_b_key in counter_b:
    if each_b_key in counter_a:
        if counter_b[each_b_key] > counter_a[each_b_key] :
            results.append(each_b_key)
    else:
        results.append(each_b_key)

results = sorted(results, key=int)
results = ' '.join(str(e) for e in results)
print(results)
